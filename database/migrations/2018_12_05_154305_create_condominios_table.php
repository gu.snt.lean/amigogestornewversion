<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondominiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condominios', function (Blueprint $table) {
            $table->smallIncrements('id')->autoIncrement();
            $table->string ('razaoSolcial',100);
            $table->string ('nomeFantasia',100);
            $table->string ('cnpj',100);
            $table->string ('longradouto',100);
            $table->string ('bairro',100);
            $table->string ('cidade',100);
            $table->string ('estado',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condominios');
    }
}
