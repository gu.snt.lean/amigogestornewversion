@extends('condominio.layout')
@section('content')

<div class="container">
<div class="card uper">
  <div class="card-header">
    Cadastro de condominio
  </div>
  <div class="card-body">    
      <form method="POST" action="{{ route('condominios.store') }}">
        @csrf
        <div class="form-group">
          <label for="name">Razão social:</label>
          <input type="text" class="form-control" name="razaoSolcial" />
        </div>
        <div class="form-group">
          <label for="name">Nome Fantasia:</label>
          <input type="text" class="form-control" name="nomeFantasia" />
        </div>
        <div class="form-group">
          <label for="name">CNPJ:</label>
          <input type="text" class="form-control" name="cnpj" id="cnpj" />
        </div>
        <div class="form-group">
          <label for="name">Longradouro:</label>
          <input type="text" class="form-control" name="longradouto" />
        </div>
        <div class="form-group">
          <label for="name">Bairro:</label>
          <input type="text" class="form-control" name="bairro" />
        </div>
        <div class="form-group">
          <label for="name">Cidade:</label>
          <input type="text" class="form-control" name="cidade" />
        </div>
        <div class="form-group">
          <label for="name">Estado:</label>
          <select class="form-control" name="estado">
              <?php $estadosBrasileiros = ["AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"]; ?>
              @foreach($estadosBrasileiros as $estado)
                <option value= {{ $estado }}> {{ $estado }} </option> 
              @endforeach                                    
          </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
        </div>
        
      </form>
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
  $("#cnpj").mask("999.999.999/9999-99");
  
</script>

@endsection