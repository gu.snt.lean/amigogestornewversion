@extends('condominio.layout')
@section('content')

<div class="container">
<div class="card uper">
  <div class="card-header">
    Cadastro de Usuario
  </div>
  <div class="card-body">    
      <form method="POST" action="{{ route('usuario.store') }}">
        @csrf
        <div class="form-group">
          <label for="name">Nome usuario:</label>
          <input type="text" class="form-control" name="name" required />
        </div>
        <div class="form-group">
          <label for="name">E-mail:</label>
          <input type="E-mail" class="form-control" name="email" required />
        </div>
        <div class="form-group">
          <label for="name">Senha:</label>
          <input type="password" class="form-control" name="password" required />
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Cadastrar</button>
        </div>
        
      </form>
  </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

@endsection