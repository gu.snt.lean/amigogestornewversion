<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
         $request->validate([
            'name'  		=> 'required',
            'email' 	 	=> 'required',
            'password'      => 'required',
        ]);


       User::create($request->all());

        return redirect()->route('condominios.index')->with('success','Atualizado com sucesso');
    }
}
