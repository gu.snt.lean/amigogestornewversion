<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condominio extends Model{
	
	public $timestamps = false;

    protected $fillable = [
    	'razaoSolcial' ,
        'nomeFantasia' ,
        'cnpj'         ,
        'longradouto'  ,
        'bairro'       ,
        'cidade'       ,
        'estado'       
    ];


}
